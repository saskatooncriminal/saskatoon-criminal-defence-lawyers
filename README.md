# Saskatoon Criminal Defence Lawyers

Linh Pham, LL.B (hons) is a criminal defense lawyer who was previously associated with Merchant Law Group LLP where he handled complex class action litigation. He now focuses on the defence of serious criminal and drug offences. 